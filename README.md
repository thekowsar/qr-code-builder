# qr-code-builder

Spring boot project for to get TLV QR code.


## Used Technology

- **Programming language**: Java
- **Framework**: Spring Boot
- **Automated build**: Apache Maven

## Prerequisites

- **JDK 11**
- **Maven**

## How To Run

1. Go to the project root directory.
2. Open terminal in this project root directory.
3. Run the following command to build project jar.
- `mvn clean compile install`
4. Run the following command to run project.
- `nohup java -jar ./target/qr-code-builder-0.0.1-SNAPSHOT.jar &`
- `tail -f nohup.out`
5. Project will run in **http://127.0.0.1:9090/**
6. To stop project run following command.
- `ps aux | grep qr-code-builder-0.0.1`
7. It will return service PID, then kill project by PID.
- `kill -9 $PID`

## API Doc

#### 2. Get TLV QR code string
**Http Method:** GET

**URL:** http://localhost:9090/qr/v1/api/get-hex-string?seller=Bobs Records&taxNumber=310122393500003&invoiceDate=2022-04-25T15:30:00Z&invoiceTotalAmount=1000.00&invoiceTaxAmount=150.00

| field | data type |description|
| ------ | ------ | ------ |
| seller | String | seller Name |
| taxNumber | String | vat registration number |
| invoiceDate | String | timestamp |
| invoiceTotalAmount | String | total amount with vat |
| invoiceTaxAmount | String | vat amount |

** Response Body **
```
{
    "status": "OK",
    "message": "ENTITY_RETRIEVED_SUCCESS",
    "data": "AQxCb2JzIFJlY29yZHMCDzMxMDEyMjM5MzUwMDAwMwMUMjAyMi0wNC0yNVQxNTozMDowMFoEBzEwMDAuMDAFBjE1MC4wMA=="
}
```


**CURL Command:**

```
curl --location --request GET 'http://localhost:9090/qr/v1/api/get-hex-string?seller=Bobs Records&taxNumber=310122393500003&invoiceDate=2022-04-25T15:30:00Z&invoiceTotalAmount=1000.00&invoiceTaxAmount=150.00'
```
