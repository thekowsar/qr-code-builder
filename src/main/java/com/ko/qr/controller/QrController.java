package com.ko.qr.controller;

import com.ko.qr.constants.ApiConstants;
import com.ko.qr.constants.QrConstants;
import com.ko.qr.dto.ResponseDto;
import com.ko.qr.dto.ResponseMessage;
import com.ko.qr.dto.ResponseStatus;
import com.ko.qr.exception.ExceptionHandlerUtil;
import com.ko.qr.service.QRBarcodeEncoderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class QrController {

    @Autowired
    QRBarcodeEncoderService qrBarcodeEncoderService;

    @GetMapping(value= QrConstants.GET_QR_HEX_STRING, produces = ApiConstants.EXTERNAL_MEDIA_TYPE)
    public ResponseEntity<ResponseDto> getAll(
            @RequestParam("seller") String seller,
            @RequestParam("taxNumber") String taxNumber,
            @RequestParam("invoiceDate") String invoiceDate,
            @RequestParam("invoiceTotalAmount") String invoiceTotalAmount,
            @RequestParam("invoiceTaxAmount") String invoiceTaxAmount

    ) throws ExceptionHandlerUtil {
        log.info("Request received for ------------------------------------------ : "
                + "\n                                                             : Seller Name: " + seller
                + "\n                                                             : Tax Number: " + taxNumber
                + "\n                                                             : Invoice Date: " + invoiceDate
                + "\n                                                             : Invoice Total Amount: " + invoiceTotalAmount
                + "\n                                                             : Invoice Tax Amount: " + invoiceTaxAmount
        );
        String base64HexString = qrBarcodeEncoderService.encode(seller, taxNumber, invoiceDate, invoiceTotalAmount, invoiceTaxAmount);
        log.info("Response Return Base64 String : ------------------------------- : "
                + "\n                                                             : Base64 String: " + base64HexString);

        return new ResponseEntity<>(
                new ResponseDto<>(
                        ResponseStatus.OK,
                        ResponseMessage.ENTITY_RETRIEVED_SUCCESS,
                        base64HexString),
                HttpStatus.OK);
    }
}
