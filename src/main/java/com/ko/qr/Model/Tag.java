package com.ko.qr.Model;

import lombok.extern.slf4j.Slf4j;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

@Slf4j
public class Tag {

    private int tag;
    private String value;

    public Tag(int tag, String value) {
        if (value == null || value.trim().equals("")) {
            throw new IllegalArgumentException("Value cannot be null or empty");
        }
        this.tag = tag;
        this.value = value;
    }

    private int getTag() {
        return this.tag;
    }

    private String getValue() {
        return this.value;
    }

    private int getLength() {
        return this.value.getBytes().length;
    }

    private String stringToHex(String str) {
        ByteBuffer buffer = StandardCharsets.UTF_8.encode(str);
        String utf8EncodedString = StandardCharsets.UTF_8.decode(buffer).toString();
        StringBuilder stringBuilder = new StringBuilder();
        char[] charArray = utf8EncodedString.toCharArray();
        for (char c : charArray) {
            String charToHex = Integer.toHexString(c);
            stringBuilder.append(charToHex);
        }
        return stringBuilder.toString();
    }

    public String numToHex(int num){
        int rem;
        String hex="";
        char hexchars[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        while(num>0)
        {
            rem=num%16;
            hex=hexchars[rem]+hex;
            num=num/16;
        }
        return hex.length() == 1 ? hexchars[0]+hex : hex;
    }

    @Override
    public String toString() {
        log.info(String.format("Tag Name: %s, Hex: %s",this.getValue(), this.numToHex(this.getTag()) + this.numToHex(this.getLength()) + this.stringToHex(this.getValue())));
        return this.numToHex(this.getTag()) + this.numToHex(this.getLength()) + this.stringToHex(this.getValue());
    }

}
