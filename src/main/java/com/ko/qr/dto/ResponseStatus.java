package com.ko.qr.dto;


public enum ResponseStatus {
    OK, // for any operation success
    FAILED, // use for unkown error
    CREATED,
    UPDATED,
    DELETED,
    NOT_FOUND
}
