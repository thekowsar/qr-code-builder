package com.ko.qr.dto;

public enum ResponseMessage {

    SUCCESSFULLY_SAVED("Successfully Saved"),
    SUCCESSFULLY_UPDATED("Successfully Updated"),
    SUCCESSFULLY_DELETED("Successfully Deleted"),
    DELETION_FAILED("Successfully Deleted"),
    ENTITY_RETRIEVED_SUCCESS("Object Retrieved Successfully"),
    NOT_FOUND("Entity Not Found");

    private final String LABEL;

    ResponseMessage(final String LABEL) {
        this.LABEL = LABEL;
    }

    @Override
    public String toString() {
        return LABEL;
    }
}
