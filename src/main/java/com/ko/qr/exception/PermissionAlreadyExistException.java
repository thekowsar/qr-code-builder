package com.ko.qr.exception;

public class PermissionAlreadyExistException extends RuntimeException {
    public PermissionAlreadyExistException(String message) {
        super(message);
    }
}
