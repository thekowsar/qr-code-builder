package com.ko.qr.constants;

import org.springframework.http.MediaType;

public interface ApiConstants {

    String EXTERNAL_MEDIA_TYPE = MediaType.APPLICATION_JSON_VALUE;

    String PRIVATE_API_ENDPOINT = "/qr/v1/api";

    String ID = "id";

    String ID_PATH_VAR = "/{" + ID + "}";
}