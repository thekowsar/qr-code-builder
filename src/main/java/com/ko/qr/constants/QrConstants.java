package com.ko.qr.constants;

import org.springframework.http.MediaType;

import static com.ko.qr.constants.ApiConstants.PRIVATE_API_ENDPOINT;

public interface QrConstants {

    String GET_QR_HEX_STRING = PRIVATE_API_ENDPOINT + "/get-hex-string";

}
