package com.ko.qr.service;

import com.ko.qr.Model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Service;

import java.math.BigInteger;


@Slf4j
@Service
public class QRBarcodeEncoderService {

    private QRBarcodeEncoderService() {
    }

    public String encode(
            String sellerStr,
            String taxNumberStr,
            String invoiceDateStr,
            String invoiceTotalAmountStr,
            String invoiceTaxAmountStr) {
        Seller seller = new Seller(sellerStr);
        TaxNumber taxNumber = new TaxNumber(taxNumberStr);
        InvoiceDate invoiceDate = new InvoiceDate(invoiceDateStr);
        InvoiceTotalAmount invoiceTotalAmount = new InvoiceTotalAmount(invoiceTotalAmountStr);
        InvoiceTaxAmount invoiceTaxAmount = new InvoiceTaxAmount(invoiceTaxAmountStr);
        return toBase64(toTLV(seller, taxNumber, invoiceDate, invoiceTotalAmount, invoiceTaxAmount));
    }

    private String toTLV(
            Seller seller,
            TaxNumber taxNumber,
            InvoiceDate invoiceDate,
            InvoiceTotalAmount invoiceTotalAmount,
            InvoiceTaxAmount invoiceTaxAmount) {

        return seller.toString()
                + taxNumber.toString()
                + invoiceDate.toString()
                + invoiceTotalAmount.toString()
                + invoiceTaxAmount.toString();
    }

    private String toBase64(String tlvString) {
        log.info("Full Hex: {}", tlvString);
        BigInteger bigint = new BigInteger(tlvString, 16);
        StringBuilder sb = new StringBuilder();
        byte[] ba = Base64.encodeInteger(bigint);
        for (byte b : ba) {
            sb.append((char)b);
        }
        String base64Str = sb.toString();
        log.info("Base 64 String: {}", base64Str);
        return base64Str;
    }

}
