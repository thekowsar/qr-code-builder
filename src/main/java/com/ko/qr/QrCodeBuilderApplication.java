package com.ko.qr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QrCodeBuilderApplication {

	public static void main(String[] args) {
		SpringApplication.run(QrCodeBuilderApplication.class, args);
	}

}
